const missions = [
    {
      id: '1',
      label: 'Mission 1',
      beginDate: '2023-08-10',
      endDate: '2023-09-15',
      missionType: 'Type A',
      freelance: {
        id: '1',
        firstname: 'John',
        lastname: 'Doe',
        email: 'john@example.com',
      },
    },
    {
      id: '2',
      label: 'Mission 2',
      beginDate: '2023-08-20',
      endDate: '2023-09-30',
      missionType: 'Type B',
      freelance: {
        id: '2',
        firstname: 'Jane',
        lastname: 'Smith',
        email: 'jane@example.com',
      },
    },
    {
      id: '2',
      label: 'Mission 3',
      beginDate: '2023-08-12',
      endDate: '2023-09-25',
      missionType: 'Type C',
      freelance: {
        id: '2',
        firstname: 'Sandy',
        lastname: 'Courgette',
        email: 'courgette@example.com',
      },
    },
   
  ];
  
  module.exports = missions;
  