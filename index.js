const express = require('express');
const cors = require('cors');
const { format } = require('date-fns'); 
const fr = require('date-fns/locale/fr'); 
const app = express();
const PORT = 3000;

app.use(cors());

const missions = require('./data/missionData');

app.get('/missions', (req, res) => {

  const groupedMissions = {
    arriving: {},
    leaving: {},
  };

  for (const mission of missions) {
    const { beginDate, endDate, freelance } = mission;

    const formattedBeginDate = format(new Date(beginDate), 'dd MMMM yyyy', { locale: fr });
    const formattedEndDate = format(new Date(endDate), 'dd MMMM yyyy', { locale: fr });

    if (!groupedMissions.arriving[formattedBeginDate]) {
      groupedMissions.arriving[formattedBeginDate] = [];
    }
    groupedMissions.arriving[formattedBeginDate].push({ ...freelance, beginDate, endDate });

    if (!groupedMissions.leaving[formattedEndDate]) {
      groupedMissions.leaving[formattedEndDate] = [];
    }
    groupedMissions.leaving[formattedEndDate].push({ ...freelance, beginDate, endDate });
  }

  res.json(groupedMissions);
});

app.listen(PORT, () => {
  console.log(`Serveur Express démarré sur le port ${PORT}`);
});
